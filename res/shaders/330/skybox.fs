#version 330 core

out vec4 FragColor;

in vec2 texCoord0;

uniform sampler2D skybox;

void main()
{
    FragColor = texture(skybox, texCoord0);
}