#version 120

// varying vec4 FragColor;

varying vec3 fragPos0;
varying vec2 texCoord0;
varying vec3 normal0;

struct Material {
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;
};

struct Light {
    vec3 position;
    
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    
    float constant;
    float linear;
    float quadratic;
};

uniform Material material;
uniform Light light;

uniform sampler2D sampler;
uniform vec3 viewPos;

void main()
{
    // // ambient lighting
    vec3 ambient = light.ambient * material.ambient;
    
    // diffuse lighting
    vec3 norm = normalize(normal0);
    vec3 lightDir = normalize(light.position - fragPos0);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = light.diffuse * (diff * material.diffuse);
    
    // specular lighting
    vec3 viewDir = normalize(viewPos - fragPos0);
    vec3 reflectDir = reflect(-lightDir, norm);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    vec3 specular = light.specular * (spec * material.specular);
    
    // attenuation
    float d = length(light.position - fragPos0);
    float attenuation = 1.0 / (light.constant + light.linear*d + light.quadratic*d*d);
    
    // phong shading
    vec3 phong = attenuation * (ambient + diffuse + specular);
    // vec3 phong = vec3(188.0/255.0, 105.0/255.0, 16.0/255.0);
    // gl_FragColor = vec4(phong, 1.0);
    gl_FragColor = vec4(vec3(fragPos0.y*1.0, 0.5, 1.0), 1.0);
    // gl_FragColor = vec4(phong, 1.0) * texture2D(sampler, texCoord0);
}
