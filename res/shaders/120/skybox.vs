#version 120

attribute vec3 position;
attribute vec2 texCoord;

varying vec2 texCoord0;

uniform mat4 projection;
uniform mat4 view;

void main()
{  
    texCoord0 = texCoord;
    vec4 pos = projection * view * vec4(position, 1.0);
    gl_Position = pos.xyww;
}