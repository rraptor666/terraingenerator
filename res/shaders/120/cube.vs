#version 120
attribute vec3 position;
attribute vec2 texCoord;

varying vec3 FragPos;
varying vec2 texCoord0;

uniform mat4 model;
uniform mat4 projection;
uniform mat4 view;

void main()
{

    vec3 FragPos = vec3(model * vec4(position, 1.0));
    gl_Position = projection * view * vec4(FragPos, 1.0);
    texCoord0 = texCoord;

}
