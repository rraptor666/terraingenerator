#version 120

attribute vec3 position;
attribute vec2 texCoord;
attribute vec3 normal;

varying vec3 fragPos0;
varying vec2 texCoord0;
varying vec3 normal0;

uniform mat4 model;
uniform mat4 projection;
uniform mat4 view;

void main()
{
    normal0 = normal;
    vec3 FragPos = vec3(model * vec4(position, 1.0));
    gl_Position = projection * view * vec4(FragPos, 1.0);
    // texCoord0 = texCoord;
    fragPos0 = position;

}
