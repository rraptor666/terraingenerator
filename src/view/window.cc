#include "window.hh"

Window::Window():
    mInited(false),
    mTitle(W_TITLE),
    mWindow(nullptr),
    mContext(NULL) {

        mInited = init();

}


Window::~Window() {
    SDL_GL_DeleteContext(mContext);
    SDL_DestroyWindow(mWindow);
    SDL_Quit();
}


void Window::swapBuffers() {
    SDL_GL_SwapWindow(mWindow);
}


void Window::clear() {
    glClearColor(0.0f, 0.25f, 0.5f, 1.0f);
    // glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}


void Window::updateFPSCounter(const float &deltaTime) {
   std::string title = mTitle + " FPS: " + std::to_string( (unsigned int)(1000.0f/deltaTime) );
   SDL_SetWindowTitle(mWindow, title.c_str());
}


bool Window::init() {

    srand(time(0));

    SDL_Init(SDL_INIT_EVERYTHING);

    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);

    SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

    mWindow = SDL_CreateWindow(mTitle.c_str(),
                               SDL_WINDOWPOS_CENTERED,
                               SDL_WINDOWPOS_CENTERED,
                               W_WIDTH,
                               W_HEIGHT,
                               SDL_WINDOW_OPENGL);

    if (mWindow == nullptr) {
        std::cerr << "Window initializaion failed.\n";
        return false;
    }

    mContext = SDL_GL_CreateContext(mWindow);
    if (glewInit() != 0) {
        std::cerr << "Glew initialization failed.\n";
        return false;
    }

    // SDL_GL_SetSwapInterval(0);

    glEnable(GL_DEPTH_TEST);
    return true;
}

