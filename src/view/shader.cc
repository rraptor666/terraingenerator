#include "shader.hh"

static void checkShaderError(unsigned int shader,
                             unsigned int flag,
                             bool program,
                             const std::string errorMSG,
                             const std::string &fileName="");

static std::string loadShader(const std::string &fileName);

static unsigned int createShader(const std::string &text,
                                 unsigned int shaderType,
                                 const std::string &fileName="");



Shader::Shader(const std::string &dir, const std::string &fsPath, const std::string &vsPath):

    mShaders{ createShader( loadShader(dir + fsPath), GL_FRAGMENT_SHADER, fsPath),
              createShader( loadShader(dir + vsPath), GL_VERTEX_SHADER, vsPath) },

    mProgram( glCreateProgram() ) {

        init();

}


Shader::~Shader() {

    for (unsigned int i=0; i<SHADER_COUNT; ++i) {
        glDetachShader(mProgram, mShaders[i]);
        glDeleteShader(mShaders[i]);
    }

    glDeleteProgram(mProgram);
}


void Shader::init() {

    for (unsigned int i=0; i<SHADER_COUNT; ++i) {
        glAttachShader(mProgram, mShaders[i]);
    }

    // TODO: is this necessary?
    glBindAttribLocation(mProgram, 0, "position");
    glBindAttribLocation(mProgram, 1, "texCoord");
    glBindAttribLocation(mProgram, 2, "normal");

    glLinkProgram(mProgram);
    checkShaderError(mProgram,
                     GL_LINK_STATUS,
                     true,
                     "Error: program linking failed");

}


void Shader::bind() {
    glUseProgram(mProgram);
}


void Shader::setPerspective(Camera *cam, glm::mat4 model, bool addViewPos) {

    if (addViewPos) {
        setVec3("viewPos", cam->getPos());
    }

    setMat4("projection", cam->getProjection());
    setMat4("view", cam->getView());
    setMat4("model", model);
}

void Shader::setSkyboxPerspective(Camera* cam, glm::mat4 model, bool addViewPos) {
    glm::mat4 view = glm::mat4(glm::mat3(cam->getView()));
    setMat4("projection", cam->getProjection());
    setMat4("view", view);
}


void Shader::setMat4(const std::string &name, const glm::mat4 &mat) {
    GLint location = glGetUniformLocation(mProgram, name.c_str());
    glUniformMatrix4fv(location, 1, GL_FALSE, &mat[0][0]);
}


void Shader::setVec3(const std::string &name, const glm::vec3 &vec) {
    GLint location = glGetUniformLocation(mProgram, name.c_str());
    glUniform3fv(location, 1, &vec[0]);
}


void Shader::setMaterial(const Material *m) {

    if (m == nullptr) {
        return;
    }

    glUniform3fv( glGetUniformLocation(mProgram, "material.ambient"),  1, &m->ambient.x );
    glUniform3fv( glGetUniformLocation(mProgram, "material.diffuse"),  1, &m->diffuse.x );
    glUniform3fv( glGetUniformLocation(mProgram, "material.specular"), 1, &m->specular.x );

    glUniform1f( glGetUniformLocation(mProgram,  "material.shininess"), m->shininess );
}


void Shader::setLight(const Light *l) {

    if (l == nullptr) {
        return;
    }

    glUniform3fv( glGetUniformLocation(mProgram, "light.position"), 1, &l->specular.x );
    glUniform3fv( glGetUniformLocation(mProgram, "light.ambient"),  1, &l->ambient.x );
    glUniform3fv( glGetUniformLocation(mProgram, "light.diffuse"),  1, &l->diffuse.x );
    glUniform3fv( glGetUniformLocation(mProgram, "light.specular"), 1, &l->specular.x );

    glUniform1f( glGetUniformLocation(mProgram, "light.constant"),  l->constant );
    glUniform1f( glGetUniformLocation(mProgram, "light.linerar"),   l->linear );
    glUniform1f( glGetUniformLocation(mProgram, "light.quadratic"), l->quadratic );
}


static void checkShaderError(unsigned int shader,
                             unsigned int flag,
                             bool program,
                             const std::string errorMSG,
                             const std::string &fileName) {

    int success = 0;
    char error[1024] = {0};

    if (program) {
        glGetProgramiv(shader, flag, &success);

    } else {
        glGetShaderiv(shader, flag, &success);
    }

    if (!success) {
        if (program) {
            glGetProgramInfoLog(shader, sizeof(error), NULL, error);

        } else {
            glGetShaderInfoLog(shader, sizeof(error), NULL, error);
        }
        std::cerr << "in file "
                  << fileName
                  << ":\n"
                  << errorMSG
                  << ": '"
                  << error
                  << "'\n\n";

    }

}


static std::string loadShader(const std::string &fileName) {

    std::ifstream file(fileName);
    std::string output = "";

    if (file.fail()) {
        std::cerr << "Failed to load "
                  << fileName
                  << std::endl;

        return output;
    }

    std::string line = "";
    while ( getline(file, line) ) {
        output.append(line + "\n");
    }

    return output;

}


static unsigned int createShader(const std::string &text,
                                 unsigned int shaderType,
                                 const std::string &fileName) {


    unsigned int shaderID = glCreateShader(shaderType);

    if (shaderID == 0) {
        std::cerr << "Shader creation failed.\n";
    }

    const char* shaderString = text.c_str();
    glShaderSource(shaderID, 1, &shaderString, NULL);
    glCompileShader(shaderID);

    checkShaderError(shaderID,
                     GL_COMPILE_STATUS,
                     false,
                     "Shader compilation failed",
                     fileName);

    return shaderID;

}


