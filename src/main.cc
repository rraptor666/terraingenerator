#include "./control/game.hh"

// TODO: Model class loads meshes with Asset (assimp/tinyobjloader)
// TODO: Mesh::initIndexBuffer


// TODO: model statistics/debug print after init( VAO etc yes/no, vertex count...)

// TODO: mesh with init options: exlude normals/texCoord etc!!!!


// BUG: something wrong with (120?) textures, test sun.fs texture2D
// without "result" -> fixed?


// TODO: reimplement genVerticesWithoutIndices using genVertices and genIndices:
// push(/insert?) vertex to genVertices according to indices

// terrain generator stand-alone------------------------------------------------
// TODO: add global constant to limit time based movements for program to work correctly w/ and w/o vsync
// TODO: give shader to model to hold
// TODO: DataVec for structs containing texcoords, normals and vertices, VertexVec for only vertices
// TODO: rename Game into MainControl, Game is for game state handling?
// TODO: fix noisegenerator to generate different noise woth different octave and roughness values (replace with actual perlinnois?)
// TODO: remake Mesh class, add NormalVec into Model params
// TODO: add flat bottom to terrain
// TODO: divide terrain into chunks(e.g. 100x100 OR 64x64 sq/chunk) -> BVH tree etc
// TODO: procedual terrain generating, starting from camera position
// -----------------------------------------------------------------------------

int main(int argc, char** argv) {
    
/*
    NoiseGenerator* nGen = NoiseGenerator::getInstance();
    std::string line;
    while (true) {
        
        if (line == "") {
            break;
        }
        
        std::vector<std::string> cmd;
        getline(std::cin, line);
        utility::split(cmd, line, " ");
        
        double x = std::stod(cmd[0]);
        double y = std::stod(cmd[1]);
        
        std::cout << nGen->noise1(x, y, 50, 1.0f) << "\n";
        
    }
    */
    Game gm(argc, argv);
    if (!gm.isInited()) {
        std::cerr << "Initialization failed.\n";
        return 0;
    }
    gm.run();

    unsigned int width = 600;
    unsigned int height = 0;
    for (unsigned int i=0; i<width * height; ++i) {
        std::cout << (float)rand() / (float)RAND_MAX << "\n";
    }
    return 0;
}
