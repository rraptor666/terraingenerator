#include "mesh.hh"


Mesh::Mesh(VertexVec &vertices):
    mVertices(vertices) {

        init();
}


Mesh::Mesh(const VertexVec &vertices):
    mVertices(vertices ) {

    init();
}


Mesh::Mesh(VertexVec &vertices, std::vector<tinyobj::index_t> &indices):
    mVertices(vertices) {

    init();

}

Mesh::Mesh(VertexVec &vertices, std::vector<unsigned int> &indices):
    mVertices(vertices),
    mIndices(indices) {

    init();

}


Mesh::Mesh(VertexVec &vertices, IndexVec &indices, TexVec &texCoords): 
    mVertices(vertices),
    mIndices(indices),
    mTexCoords(texCoords) {

        init();
}


Mesh::Mesh(VertexVec &vertices, IndexVec &indices, NormalVec &normals):
    mVertices(vertices),
    mIndices(indices),
    mNormals(normals) {
        
        std::vector<glm::vec3> positions;
        std::vector<glm::vec2> texCoords;

        positions.reserve(mVertices.size());
        texCoords.reserve(mVertices.size());

        for (unsigned int i=0; i<mVertices.size(); ++i) {
            positions.push_back( mVertices[i]->pos );
            texCoords.push_back( mVertices[i]->tex );
        }

        glGenVertexArrays(1, &mVAO);
        glBindVertexArray(mVAO);

        glGenBuffers(1, &mVBO);
        glGenBuffers(1, &mTBO);
        glGenBuffers(1, &mIBO);
        glGenBuffers(1, &mNBO);

        initPos(positions);
        initTex(texCoords);
        initIndices();
        // initNor();

       glBindVertexArray(0);    
    
}




Mesh::~Mesh() {

    for (int i=mVertices.size()-1; i>=0; --i) {
        mVertices.pop_back();
    }

    glDeleteVertexArrays(1, &mVAO);
    glDeleteVertexArrays(1, &mLightVAO);

    glDeleteBuffers(1, &mVBO);
    glDeleteBuffers(1, &mTBO);
    glDeleteBuffers(1, &mIBO);
    glDeleteBuffers(1, &mNBO);

}

void Mesh::draw()
{
    // glBindVertexArray(mLightVAO);
    glBindVertexArray(mVAO);

    if (mIndices.size() > 0) {
        glDrawElements(GL_TRIANGLES, mIndices.size(), GL_UNSIGNED_INT, 0);
    } else {
        glDrawArrays(GL_TRIANGLES, 0, mVertices.size());
    }
    glBindVertexArray(0);
}


unsigned int Mesh::getVertexCount() { return mVertices.size(); }
unsigned int Mesh::getIndexCount() { return mIndices.size(); }
unsigned int Mesh::getVAO() { return mVAO; }
unsigned int Mesh::getLightVAO() { return mLightVAO; }


VertexVec* Mesh::getVertices() { return &mVertices; }
IndexVec* Mesh::getIndices() { return &mIndices; }


void Mesh::setNormals(std::vector<glm::vec3> &vec) {
    mNormals = vec;
    initNor();
}


void Mesh::init() {

    std::vector<glm::vec3> positions;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texCoords;

    positions.reserve(mVertices.size());
    // normals.reserve(mVertices.size());

    if (mTexCoords.size() == 0) {
        texCoords.reserve(mVertices.size());
    }


    for (unsigned int i=0; i<mVertices.size(); ++i) {
        positions.push_back( mVertices[i]->pos );
        // normals.push_back( mVertices[i]->nor );

        if (mTexCoords.size() == 0) {
            texCoords.push_back( mVertices[i]->tex );
        }
    }


    glGenVertexArrays(1, &mVAO);
    glBindVertexArray(mVAO);

    glGenBuffers(1, &mVBO);
    glGenBuffers(1, &mTBO);
    glGenBuffers(1, &mIBO);

    // VBO
    initPos(positions);


    // lightVBO
    // initLightVBO();

    // textures
    if (mTexCoords.size() > 0) {
        initTex(mTexCoords);
    
    } else {
        initTex(texCoords);
    }

    if (mIndices.size() > 0) {
        initIndices();
    }
    // TODO: does this need normals?
    // normals
    // initNor();

   glBindVertexArray(0);


}


void Mesh::initLightVBO() {

    glGenVertexArrays(1, &mLightVAO);
    glBindVertexArray(mLightVAO);
    glBindBuffer(GL_ARRAY_BUFFER, mVBO);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);   
}


void Mesh::initPos(std::vector<glm::vec3> &vertices) {

    glBindBuffer(GL_ARRAY_BUFFER, mVBO);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(vertices[0]), &vertices[0], GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);   
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
}


void Mesh::initTex(std::vector<glm::vec2> &texCoords) {

    glBindBuffer(GL_ARRAY_BUFFER, mTBO);
    glBufferData(GL_ARRAY_BUFFER, texCoords.size() * sizeof(texCoords[0]), &texCoords[0], GL_STATIC_DRAW);

    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);
}


void Mesh::initNor() {
    glBindBuffer(GL_ARRAY_BUFFER, mNBO);
    glBufferData(GL_ARRAY_BUFFER, mNormals.size() * sizeof(mNormals[0]), &mNormals[0], GL_STATIC_DRAW);
    
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(glm::vec3), (void*)(3 * sizeof(float)));
}

void Mesh::initIndices(std::vector<tinyobj::index_t> &vec) {
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, vec.size() * sizeof(vec[0]), &vec[0], GL_STATIC_DRAW);
}


void Mesh::initIndices() {
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, mIndices.size()*sizeof(mIndices[0]), &mIndices[0], GL_STATIC_DRAW);
}


