#ifndef MESH_HH
#define MESH_HH

#include "tiny_obj_loader.h"
#include "../misc/datastructures.hh"
#include "../view/shader.hh"

#include <GL/glew.h>
#include <iostream>
#include <string>
#include <vector>

class Mesh {

public:

    Mesh(VertexVec &vertices);
    Mesh(const VertexVec &vertices);
    Mesh(VertexVec &vertices, IndexVec &indices);
    Mesh(VertexVec &vertices, std::vector<tinyobj::index_t> &indices);

    Mesh(VertexVec &vertices, IndexVec &indices, TexVec &texCoords);
    Mesh(VertexVec &vertices, IndexVec &indices, NormalVec &normals);

    ~Mesh();

    void draw();

    unsigned int getVertexCount();
    unsigned int getIndexCount();

    unsigned int getVAO();
    unsigned int getLightVAO();

    VertexVec* getVertices();
    IndexVec* getIndices();

    void setNormals(std::vector<glm::vec3> &vec);



private:

    void init();
    void initLightVBO();
    void initPos(std::vector<glm::vec3> &vertices);
    void initTex(std::vector<glm::vec2> &texCoords);
    void initNor();

    void initIndices(std::vector<tinyobj::index_t> &vec);
    void initIndices();

    // vertex array object
    unsigned int mVAO;
    unsigned int mLightVAO;

    unsigned int mVBO;  // vertex buffer
    unsigned int mTBO;  // texcoord buffer
    unsigned int mIBO;  // index buffer
    unsigned int mNBO;  // normal buffer
    
    VertexVec mVertices;
    IndexVec mIndices;
    TexVec mTexCoords;
    NormalVec mNormals;


};

#endif // MESH_HH
