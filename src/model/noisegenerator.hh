#ifndef NOISEGENERATOR_HH
#define NOISEGENERATOR_HH

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <random>
#include <iostream>  

class NoiseGenerator {
  
public:
    static NoiseGenerator* getInstance();

    // originals made by Javidx9, available at:
    //https://github.com/OneLoneCoder/videos/blob/master/OneLoneCoder_PerlinNoise.cpp
    
    void noiseArray1D(float* noiseArray,
                        float* seedArray,
                        const unsigned int size, 
                        float roughness=2.0f,
                        unsigned int octaves=4);
    
    
    void noiseArray2D(float* noiseArray,
                      float* seedArray,
                      const unsigned int width, 
                      const unsigned int height,
                      float roughness=2.0f,
                      unsigned int octaves=4);
    
    // original by ThinMatrix, available at:
    // https://www.youtube.com/watch?v=qChQrNWU9Xw&list=PLRIWtICgwaX0u7Rf9zkZhLoLuZVfUksDP&index=37
    float noise1(int x, int z, int sideVertices, 
                 float amplitude=75.0f, float roughness=2.0f, 
                 int octaves=4);
            
    float noiseFromImage(); // TODO?        



private:
    NoiseGenerator();
    
    float getInterpolatedNoise(float x, float z);
    float interpolate(float a, float b, float blend);
    float getSmoothNoise(int x, int z);
    float getNoise(int x, int z);
    
    static NoiseGenerator* mInstance;    
    
};


#endif