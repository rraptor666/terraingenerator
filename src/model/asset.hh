#ifndef ASSET_HH
#define ASSET_HH

#include "stb_image.h"
#include "../misc/utility.hh"

#include <fstream>
#include <GL/glew.h>
#include <iostream>
#include <string>
#include <SDL2/SDL.h>
#include <vector>


class Asset {

public:

    static unsigned int loadTexture(const std::string &path, bool verticalFlip=true);
    static unsigned int loadCubeMap(const std::vector<std::string> &paths,
                             bool addAlpha=false);
    
    static void loadCubeMap(const std::vector<std::string> &paths, std::vector<unsigned int> &texVec);
    
    static SDL_Surface* loadHeightMap(const std::string &path);

};

#endif // ASSET_HH
