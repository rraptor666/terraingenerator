#include "model.hh"
#include "tiny_obj_loader.h"

Model::Model(VertexVec &vertices,
             const glm::vec3 &pos,
             const glm::vec3 &rot,
             const glm::vec3 &scale,
             const unsigned int tex):

    mMeshes{ new Mesh(vertices) },
    mPos(pos),
    mRot(rot),
    mScale(scale),
    mOrigPos(pos),
    mTexture(tex),
    mMaterial(nullptr),
    mLight(nullptr),
    mShader(nullptr),
    mDistance( sqrt(mPos.x*mPos.x + mPos.y*mPos.y + mPos.z*mPos.z) ) {

}

Model::Model(VertexVec &vertices,
             IndexVec &indices,
             const glm::vec3 &pos,
             const glm::vec3 &rot,
             const glm::vec3 &scale,
             const unsigned int tex):

    mMeshes{ new Mesh(vertices, indices) },
    mPos(pos),
    mRot(rot),
    mScale(scale),
    mOrigPos(pos),
    mTexture(tex),
    mMaterial(nullptr),
    mLight(nullptr),
    mShader(nullptr),
    mDistance( sqrt(mPos.x*mPos.x + mPos.y*mPos.y + mPos.z*mPos.z) ) {


        for (unsigned int i=0; i<mMeshes.size(); ++i) {
            mStat.vSize += mMeshes[i]->getVertexCount();
            mStat.iSize += mMeshes[i]->getIndexCount();
            mStat.byteSize += sizeof( *mMeshes[i] );
        }


}


Model::Model(VertexVec &vertices,
             IndexVec &indices,
             NormalVec &normals,
             const glm::vec3 &pos,
             const glm::vec3 &rot,
             const glm::vec3 &scale,
             const unsigned int tex):

    mMeshes{ new Mesh(vertices, indices, normals) },
    mPos(pos),
    mRot(rot),
    mScale(scale),
    mOrigPos(pos),
    mTexture(tex),
    mMaterial(nullptr),
    mLight(nullptr),
    mShader(nullptr),
    mDistance( sqrt(mPos.x*mPos.x + mPos.y*mPos.y + mPos.z*mPos.z) ) {

}


Model::~Model() {

    for (unsigned int i=0; i<mMeshes.size(); ++i) {
        delete mMeshes[i];
    }

    // delete mShader;

}


void Model::draw(Camera* cam) {

    mShader->bind();
    mShader->setMaterial(mMaterial);
    mShader->setLight(mLight);
    mShader->setPerspective(cam, getModel(), false);

    if (mTexture != 0) {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, mTexture);
    }


    for (unsigned int i=0; i<mMeshes.size(); ++i) {
        mMeshes[i]->draw();
    }

    // deactivate texture
    if (mTexture != 0) {
        glBindTexture(GL_TEXTURE_2D, 0);
    }


}

glm::mat4 Model::getModel() const {
    glm::mat4 trans;

    // change position
    trans = glm::translate(trans, mPos);

    // change angle
    trans = glm::rotate(trans, mRot.x, glm::vec3(1, 0, 0));
    trans = glm::rotate(trans, mRot.y, glm::vec3(0, 1, 0));
    trans = glm::rotate(trans, mRot.z, glm::vec3(0, 0, 1));

    // change scaling
    trans = glm::scale(trans, mScale);

    return trans;
}


glm::vec3 Model::getPos() const { return mPos; }
glm::vec3 Model::getRot() const { return mRot; }
glm::vec3 Model::getScale() const { return mScale; }
MeshVec Model::getMeshes() const { return mMeshes; }
Stat* Model::getStat() { return &mStat; }
Material* Model::getMaterial() { return mMaterial; }
Light* Model::getLight() { return mLight; }

void Model::setPos(glm::vec3 &newPos) { mPos = newPos; }
void Model::setRot(glm::vec3 &newRot) { mRot = newRot; }
void Model::setScale(glm::vec3 &newScale) { mScale = newScale; }
void Model::setMaterial(Material *m) { mMaterial = m; }
void Model::setLight(Light *l) { mLight = l; }

void Model::setNormalsAt(NormalVec &nVec, unsigned int index) {
    
    if (index >= mMeshes.size()) {
        std::cerr << "Index out of bounds!\n";
        return;
    }
    mMeshes[index]->setNormals(nVec);
}

void Model::loadMeshes(const std::string &file) {

    tinyobj::attrib_t attr;
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;
    std::string error;

    bool status = tinyobj::LoadObj(&attr, &shapes, &materials, &error,
                                   file.c_str(), NULL, true);

    if (!error.empty()) {
        std::cerr << error << std::endl;
    }

    if (!status) {
        std::cerr << error << std::endl;
        return;
    }

    for (size_t i=0; i<shapes.size(); ++i) {

        VertexVec vertices;
        std::vector<tinyobj::index_t> indices;

        size_t index_offset = 0;
        for (size_t j=0; j<shapes[i].mesh.num_face_vertices.size(); ++j) {

            unsigned int faceVertexCount = shapes[i].mesh.num_face_vertices[j];
            for (size_t k=0; k<faceVertexCount; ++k) {

                tinyobj::index_t idx = shapes[i].mesh.indices[index_offset + k];

                tinyobj::real_t vx = attr.vertices[3*idx.vertex_index+0];
                tinyobj::real_t vy = attr.vertices[3*idx.vertex_index+1];
                tinyobj::real_t vz = attr.vertices[3*idx.vertex_index+2];

                tinyobj::real_t nx = attr.normals[3*idx.normal_index+0];
                tinyobj::real_t ny = attr.normals[3*idx.normal_index+1];
                tinyobj::real_t nz = attr.normals[3*idx.normal_index+2];

                tinyobj::real_t tx = attr.texcoords[3*idx.texcoord_index+0];
                tinyobj::real_t ty = attr.texcoords[3*idx.texcoord_index+1];

                vertices.push_back( std::make_shared<Vertex>(Vertex{ {vx, vy, vz}, {tx, ty}, {nx, ny, nz} }) );
                indices.push_back(idx);

            }
            index_offset += faceVertexCount;

        }
        mMeshes.push_back( new Mesh(vertices, indices) );

    }
}

void Model::update(float dTime, const glm::vec3 &dPos) {
    
    if (dPos.x != 0.0f) {
        mPos.x = dPos.x;
    }
    if (dPos.y != 0.0f) {
        mPos.y = dPos.y;
    }
    if (dPos.z != 0.0f) {
        mPos.z = dPos.z;
    }
    
}


void Model::setShader(Shader* s) {
    mShader = s;
}


Shader* Model::getShader() {
    return mShader;
}

