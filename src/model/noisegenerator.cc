#include "noisegenerator.hh"

NoiseGenerator* NoiseGenerator::mInstance = nullptr;

NoiseGenerator::NoiseGenerator() {
    
}


NoiseGenerator* NoiseGenerator::getInstance() {
    
    if (mInstance == nullptr) {
        mInstance = new NoiseGenerator;
    }
    
    return mInstance;
}


void NoiseGenerator::noiseArray1D(float* noiseArray,
                                    float* seedArray,
                                    const unsigned int size, 
                                    float roughness,
                                    unsigned int octaves) {

    float bias = roughness;
    if (bias < 0.2f) {
        bias = 0.2f;
    }

    // generate seed for noise
    for (unsigned int i=0; i<size; ++i) {
        seedArray[i] = (float)rand() / (float)RAND_MAX;
    }

    // generate Perlin noise approximation
    for (unsigned int i=0; i<size; ++i) {
        
            		
    		float noise = 0.0f;
    		float scaleAcc = 0.0f;
    		float scale = 1.0f;

    		for (unsigned int j=0; j<octaves; ++j) {
                
    			int pitch = size >> j;
                // std::cout << i << ", " << j << ", " << k << "\n";
                
                // prevent divide by zero
                if (pitch == 0) {
                    continue;
                }
                          
    			int sample1 = (i / pitch) * pitch;
    			int sample2 = (sample1 + pitch) % size;		
                			
    			float blend = (float)(i - sample1) / (float)pitch;
    			float sample = (1.0f - blend) * seedArray[sample1] + blend * seedArray[sample2];

    			scaleAcc += scale;
    			noise += sample * scale;
    			scale = scale / bias;
    		}

    		// Scale to seed range
    		noiseArray[i] = noise / scaleAcc;
    	}
    
    
}

             
void NoiseGenerator::noiseArray2D(float* noiseArray,
                                  float* seedArray,
                                  const unsigned int width, 
                                  const unsigned int height,
                                  float roughness,
                                  unsigned int octaves) {
    
    float bias = roughness;
    if (bias < 0.2f) {
        bias = 0.2f;
    }
    std::cout << "roughness: " << bias << ", octaves: " << octaves << "\n";
    
        // generate seed for noise [0,1]
    // for (unsigned int i=0; i<width * height; ++i) {
    //     seedArray[i] = (float)rand() / (float)RAND_MAX;
    // }

    
    // generate Perlin noise approximation
	for (unsigned int i=0; i<width; ++i) {
        
		for (unsigned int j=0; j<height; ++j) {		
            		
			float noise = 0.0f;
			float scaleAcc = 0.0f;
			float scale = 1.0f;

			for (unsigned int k=0; k<octaves; ++k) {
                
				int pitch = width >> k;
                
                // prevent divide by zero
                if (pitch == 0) {
                    continue;
                }
                          
				int sampleX1 = (i / pitch) * pitch;
				int sampleY1 = (j / pitch) * pitch;
				
				int sampleX2 = (sampleX1 + pitch) % width;					
				int sampleY2 = (sampleY1 + pitch) % width;

				float blendX = (float)(i - sampleX1) / (float)pitch;
				float blendY = (float)(j - sampleY1) / (float)pitch;
                
                unsigned int index_1 = sampleY1 * width + sampleX1;
                unsigned int index_2 = sampleY1 * width + sampleX2;
                unsigned int index_3 = sampleY2 * width + sampleX1;
                unsigned int index_4 = sampleY2 * width + sampleX2;
                
                unsigned int size = width*height;
                if (index_1 >= size) {
                    index_1 = size-1;
                }
                if (index_2 >= size) {
                    index_2 = size-1;
                }
                if (index_3 >= size) {
                    index_3 = size-1;
                }
                if (index_4 >= size) {
                    index_4 = size-1;
                }
                
				float sampleT = (1.0f - blendX) * seedArray[index_1] + blendX * seedArray[index_2];
				float sampleB = (1.0f - blendX) * seedArray[index_3] + blendX * seedArray[index_4];

				scaleAcc += scale;
				noise += (blendY * (sampleB - sampleT) + sampleT) * scale;
				scale /= bias;
			}

			// Scale to seed range
			noiseArray[j * width + i] = noise / scaleAcc;
		}
        
    }        
    
}
    
    
float NoiseGenerator::noise1(int x, int z, int sideVertices, float amplitude, float roughness, int octaves) {
    
    float total = 0;
    float d = (float)glm::pow(2, octaves-1);
    
    for(int i=0; i<octaves; ++i){
        float freq = (float)(glm::pow(2, i) / d);
        float amp = (float)glm::pow(roughness, i) * amplitude;
        
        total += getInterpolatedNoise((x + sideVertices)*freq, (z + sideVertices)*freq) * amp;
    }
    
    return total;
}
 
float NoiseGenerator::getInterpolatedNoise(float x, float z){
    int intX = (int)x;
    int intZ = (int)z;
    float fracX = x - intX;
    float fracZ = z - intZ;
     
    float v1 = getSmoothNoise(intX, intZ);
    float v2 = getSmoothNoise(intX + 1, intZ);
    float v3 = getSmoothNoise(intX, intZ + 1);
    float v4 = getSmoothNoise(intX + 1, intZ + 1);
    float i1 = interpolate(v1, v2, fracX);
    float i2 = interpolate(v3, v4, fracX);
    
    return interpolate(i1, i2, fracZ);
}
 
float NoiseGenerator::interpolate(float a, float b, float blend){
    double theta = blend * glm::pi<double>();
    float f = (1.0f - cos(theta)) * 0.5f;
    
    return a * (1.0f - f) + b * f;
}

float NoiseGenerator::getSmoothNoise(int x, int z) {
    float corners = (getNoise(x - 1, z - 1) + getNoise(x + 1, z - 1) + 
                     getNoise(x - 1, z + 1) + getNoise(x + 1, z + 1)) / 16.0f;
            
    float sides = (getNoise(x - 1, z) + getNoise(x + 1, z) + 
                   getNoise(x, z - 1) + getNoise(x, z + 1)) / 8.0f;
            
    float center = getNoise(x, z) / 4.0f;
    
    return corners + sides + center;
}

float NoiseGenerator::getNoise(int x, int z) {
    return (rand()%100) * 0.01f  * 2.0f - 1.0f;
}

