#ifndef TERRAINGENERATOR_HH
#define TERRAINGENERATOR_HH

#include "noisegenerator.hh"
#include "asset.hh"
#include "../misc/datastructures.hh"
#include "../misc/utility.hh"

#include <glm/ext.hpp>
#include <glm/glm.hpp>
#include <iostream>


const float SQ_SIDE_LENGTH = 20.0f;  // length of side of a square
const float MAX_HEIGHT = 0.5f;       // maximum height for terrain
const float MIN_HEIGHT = 0.0f;       // minimum height for terrain
const float NOISE_CONST = 0.2f;      // determines strength of noise
 
/**
 * @brief TerrainGenerator class, a singleton for generating terrain on xz plane
 */
class TerrainGenerator {

public:
    static TerrainGenerator* getInstance();

    /**
     * @brief genTerrain, generates vertices of terrain made according to parameters
     * @param vec, vector which will be populated with terrain vertices
     * @param squaresPerSide, number of squares (and vertices) on each terrain edge
     * @param center, center vertex of terrain, (0,0,0) by default
     */
    void genTerrain(VertexVec &vec, 
                    const unsigned int squaresPerSide, 
                    const glm::vec3 &center=glm::vec3(0.0f));


    /**
     * @brief genTerrain, same as the other genTerrain but for generating terrain
     *        with indices
     * @param iVec, vector to be filled with indices
     */        
    void genTerrain(VertexVec &vVec,
                    IndexVec &iVec,
                    const float terrainWidth=20.0f,
                    const float roughness=2.0f,
                    const unsigned int resolution=100,
                    const unsigned int octaves=4);

    void genSphereCube(VertexVec &vVec, 
                       IndexVec &iVec,
                       const unsigned int edgeVertices,
                       const float vertexDist=1.0f);
                       

    void addTopography(VertexVec &vVec,
                       const unsigned int verticesPerEdge,
                       const float roughness,
                       const unsigned int octaves);

    void setNoiseFactor(float noise);

private:
    TerrainGenerator();
    void genTerrainVertices(VertexVec &vVec);
    void genTerrainIndices(IndexVec &iVec);                            
    
    static TerrainGenerator* mInstance;
    
    unsigned int mSideSq;   // number of squares per side
    float mSqSideLength;
    float mRoughness;       // higher = more rough terrain, noise modifier
    unsigned int mOctaves;  // noise modifier
    float mNoise;
    

    
};


#endif