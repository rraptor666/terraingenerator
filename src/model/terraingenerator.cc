#include "terraingenerator.hh"

TerrainGenerator* TerrainGenerator::mInstance = nullptr;

TerrainGenerator::TerrainGenerator():
    mNoise(0.0f) {
}


TerrainGenerator* TerrainGenerator::getInstance() {

    if (mInstance == nullptr) {
        mInstance = new TerrainGenerator;
    }

    return mInstance;
}



// TODO: add noise to y coords, use NoiseGenerator::noise2D to get same values for vertex duplicates
void TerrainGenerator::genTerrain(VertexVec &vec, 
                                  const unsigned int squaresPerSide, 
                                  const glm::vec3 &center) {

    glm::vec3 corner_0(center.x - (squaresPerSide) * SQ_SIDE_LENGTH/2, 
                       center.y,
                       center.z - (squaresPerSide) * SQ_SIDE_LENGTH/2);
                       
                       
    // vertical axis
    float vOffset = 0.0f;                   
    for (unsigned int i=0; i<squaresPerSide; ++i) {
        
        // horizontal axis
        float hOffset = 0.0f;
        for (unsigned int j=0; j<squaresPerSide; ++j) {
            
            /*
                0----1
                |  \ |
                3----2
            */
            glm::vec3 vertex_0(corner_0.x + hOffset, 
                               corner_0.y, 
                               corner_0.z + vOffset);
                               
            glm::vec3 vertex_1(corner_0.x + hOffset + SQ_SIDE_LENGTH, 
                               corner_0.y, 
                               corner_0.z + vOffset);
                               
            glm::vec3 vertex_2(corner_0.x + hOffset + SQ_SIDE_LENGTH, 
                               corner_0.y, 
                               corner_0.z + vOffset + SQ_SIDE_LENGTH);
                               
            glm::vec3 vertex_3(corner_0.x + hOffset, 
                               corner_0.y, 
                               corner_0.z + vOffset + SQ_SIDE_LENGTH);
            
            
            // right triangle
            vec.push_back( std::make_shared<Vertex> (Vertex(vertex_0, glm::vec2(0.0f, 0.0f), {})) );
            vec.push_back( std::make_shared<Vertex> (Vertex(vertex_1, glm::vec2(1.0f, 0.0f), {})) );
            vec.push_back( std::make_shared<Vertex> (Vertex(vertex_2, glm::vec2(1.0f, 1.0f), {})) );

            // left triangle
            vec.push_back( std::make_shared<Vertex> (Vertex(vertex_2, glm::vec2(1.0f, 1.0f), {})) );
            vec.push_back( std::make_shared<Vertex> (Vertex(vertex_3, glm::vec2(0.0f, 1.0f), {})) );
            vec.push_back( std::make_shared<Vertex> (Vertex(vertex_0, glm::vec2(0.0f, 0.0f), {})) );
            
            
            // add horizontal offset
            hOffset += SQ_SIDE_LENGTH;
        }
        
        
        // add vertical offset
        vOffset += SQ_SIDE_LENGTH;
    }    
    

}


void TerrainGenerator::genTerrain(VertexVec &vVec,
                                  IndexVec &iVec,
                                  const float terrainWidth, 
                                  const float roughness,
                                  const unsigned int resolution,
                                  const unsigned int octaves) {
    
    mSqSideLength = 100.0f/resolution;
    mSideSq = terrainWidth / mSqSideLength;
    mRoughness = roughness;
    mOctaves = octaves;
    
    genTerrainVertices(vVec);
    genTerrainIndices(iVec);    
    addTopography(vVec, mSideSq+1, mRoughness, mOctaves);                        
    
    // TODO: to print info
    unsigned int vSide = mSideSq+1;
    unsigned int indexCount = mSideSq * mSideSq * 6;
    
    std::cout << "Vertices: "  << vSide << "^2 = " << vSide * vSide
              << ", "; 
    vSide * vSide == vVec.size() ? std::cout << "true" : std::cout << "false";
               
    std::cout << "\nIndices: " << mSideSq << "^2 * " << 6
              << " = " <<  indexCount << ", ";

    indexCount == iVec.size() ? std::cout << "true" : std::cout << "false";
    std::cout  << "\n\n";        

    
}


void TerrainGenerator::genTerrainVertices(VertexVec &vVec) {

    unsigned int sideVertices = mSideSq + 1;                                      
    glm::vec3 center(0.0f);

    // top left corner vertex                
    glm::vec3 corner_0(center.x - (mSideSq) * mSqSideLength/2, 
                       center.y,
                       center.z - (mSideSq) * mSqSideLength/2);
                       
                       
    // vertical axis
    float vOffset = 0.0f;                   
    for (unsigned int i=0; i<sideVertices; ++i) {
        
        // horizontal axis
        float hOffset = 0.0f;
        for (unsigned int j=0; j<sideVertices; ++j) {
            
            glm::vec3 v(corner_0.x + hOffset, corner_0.y, corner_0.z + vOffset);
            float u_coord = (rand() % 101) / 100.0f;
            float v_coord = (rand() % 101) / 100.0f;
            vVec.push_back( std::make_shared<Vertex> (Vertex(v, glm::vec2(u_coord, v_coord), {})) );
            
            hOffset += mSqSideLength;
        }
        vOffset += mSqSideLength;
    }  
    
                            
}


void TerrainGenerator::genTerrainIndices(IndexVec &iVec) {

    unsigned int offset = mSideSq + 1;       
    
    /*
        vertex order:
        
        0---1,5
        |  / |
       2,3---4
    */                                
                  
    // NOTE: if loops are counted in little squares per terrain plane side and
    // and offset in vertices per side, there's no need for edge cases                      
    for (unsigned int i=0; i<mSideSq; ++i) {
        for (unsigned int j=0; j<mSideSq; ++j) {
            
            // left triangle
            iVec.push_back( i*offset + j );
            iVec.push_back( i*offset + j+1 );
            iVec.push_back( (i+1)*offset + j );
            
            // right triangle
            iVec.push_back( (i+1)*offset + j );
            iVec.push_back( (i+1)*offset + j+1 );
            iVec.push_back( i*offset + j+1 );
        }
    }                                         
}


void TerrainGenerator::addTopography(VertexVec &vVec, 
                                     const unsigned int verticesPerEdge,
                                     const float roughness,
                                     const unsigned int octaves) {
                                         
    
    NoiseGenerator* nGen = NoiseGenerator::getInstance();
    
    
    for (unsigned int i=0; i<vVec.size(); ++i) {
        
        if (roughness < 0.0f) {
            vVec[i]->pos.y = 0.0f;                
        } else {
            float height = nGen->noise1( (int)vVec[i]->pos.x, (int)vVec[i]->pos.z, 
                                          verticesPerEdge, MAX_HEIGHT, roughness, octaves );

            // std::cout << height << "\n";                                        
            if (height < 0.0f) height = 0.0f;
            
            vVec[i]->pos.y = height*mNoise; // NOISE_CONST;
        }
    }

    
    
}


void TerrainGenerator::setNoiseFactor(float noise) {
    mNoise = noise;
}


