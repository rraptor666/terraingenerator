#ifndef MODEL_HH
#define MODEL_HH

#include "asset.hh"
#include "mesh.hh"
#include "../control/camera.hh"
#include "../view/shader.hh"

#include <GL/glew.h>
#include <glm/ext.hpp>
#include <glm/glm.hpp>
#include <vector>



class Model {

public:

      Model(VertexVec &vertices,
            const glm::vec3 &pos=glm::vec3(),
            const glm::vec3 &rot=glm::vec3(),
            const glm::vec3 &scale=glm::vec3(1.0f, 1.0f, 1.0f),
            const unsigned int tex=0);

      // with indices
      Model(VertexVec &vertices,
            IndexVec &indices,
            const glm::vec3 &pos=glm::vec3(),
            const glm::vec3 &rot=glm::vec3(),
            const glm::vec3 &scale=glm::vec3(1.0f, 1.0f, 1.0f),
            const unsigned int tex=0);

      Model(VertexVec &vertices,
            IndexVec &indices,
            NormalVec &normals,
            const glm::vec3 &pos=glm::vec3(0.0f),
            const glm::vec3 &rot=glm::vec3(0.0f),
            const glm::vec3 &scale=glm::vec3(1.0f, 1.0f, 1.0f),
            const unsigned int tex=0);


      virtual ~Model();

      virtual void draw(Camera* cam);
      virtual void update(float dTime, const glm::vec3 &dPos);

      glm::mat4 getModel() const;
      glm::vec3 getPos() const;
      glm::vec3 getRot() const;
      glm::vec3 getScale() const;
      MeshVec getMeshes() const;
      Stat* getStat();
      Material* getMaterial();
      Light* getLight();

      void setPos(glm::vec3 &newPos);
      void setRot(glm::vec3 &newRot);
      void setScale(glm::vec3 &newScale);
      void setMaterial(Material *m);
      void setLight(Light *l);
      void setNormalsAt(NormalVec &nVec, unsigned int index);

      void setShader(Shader* s);
      Shader* getShader();



protected:

    MeshVec mMeshes;
    glm::vec3 mPos;
    glm::vec3 mRot;
    glm::vec3 mScale;

    glm::vec3 mOrigPos;
    unsigned int mTexture;
    Material* mMaterial;
    Light* mLight;
    Stat mStat;

    Shader* mShader;


private:

    void loadMeshes(const std::string &file);

    float mDistance;
    float mRotOffSet;



};

#endif // MODEL_HH
