#include "asset.hh"
#include <iostream>


unsigned int Asset::loadTexture(const std::string &path, bool verticalFlip) {

    // NOTE: stbi_load takes these as ints, not uints
    int width = 0;
    int height = 0;
    int channelCount = 0;

    unsigned int textureID = 0;
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_2D, textureID);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    // glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    stbi_set_flip_vertically_on_load(verticalFlip);
    unsigned char* img = stbi_load(path.c_str(), &width, &height, &channelCount, 0);

    if (img == NULL) {
        std::cerr << "Texture "
                  << path
                  << " loading failed.\n";

    } else {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, img);
        glGenerateMipmap(GL_TEXTURE_2D);
    }

    stbi_image_free(img);
    // std::cout << "texture: " << textureID << std::endl;

    return textureID;

}


unsigned int Asset::loadCubeMap(const std::vector<std::string> &paths,
                                bool addAlpha) {

    int width = 0;
    int height = 0;
    int channelCount = 0;

    unsigned int textureID = 0;
    glGenTextures(1, &textureID);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

    stbi_set_flip_vertically_on_load(false);
    unsigned char* img = nullptr;
    for (unsigned int i=0; i<paths.size(); ++i) {

        stbi_set_flip_vertically_on_load(false);
        img = stbi_load(paths[i].c_str(), &width, &height, &channelCount, 0);

        if (img == NULL) {
            std::cerr << "Texture "
                      << paths[i]
                      << " loading failed.\n";
        } else {

            if (addAlpha) {
                glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, img);
            } else {
                glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, img);
            }
        }

        stbi_image_free(img);

    }

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    return textureID;
}


void Asset::loadCubeMap(const std::vector<std::string> &paths, std::vector<unsigned int> &texVec) {

    // std::vector<bool> flips = {true, true, false, false, true, true};
    std::vector<bool> flips = {
        true,   // back +
        true,   // front+
        false,   // right
        false,  // left
        true,   // bottom+
        false    // top+
    };
    
    for (unsigned int i=0; i<paths.size(); ++i) {
        texVec.push_back( loadTexture(paths[i], flips[i]) );
    }

}


SDL_Surface* Asset::loadHeightMap(const std::string &path) {
    
    SDL_Surface* img = SDL_LoadBMP(path.c_str());
    if (!img) {
        std::cerr << "Failed to load heightmap "
                  << path
                  << "!\n";

        return nullptr;              
    }
    
    return img;
    
}