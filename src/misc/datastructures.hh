#ifndef DATASTRUCTURES_HH
#define DATASTRUCTURES_HH

#include <glm/glm.hpp>
#include <memory>
#include <unordered_map>
#include <vector>

/**
 * Datastructures
 * 
 * Header containing globally used stuctures
*/

class Vertex {

public:

    Vertex(glm::vec3 &pos): 
        pos(pos) { 
    };
    
    Vertex(glm::vec3 pos, glm::vec2 tex, glm::vec3 nor): 
        pos(pos),
        tex(tex),
        nor(nor) {
    };


    glm::vec3 pos;
    glm::vec2 tex;
    glm::vec3 nor;

    Vertex& operator= (const Vertex &a) {
        pos = a.pos;
        tex = a.tex;
        nor = a.nor;

        return *this;
    }

};

struct Stat {
        unsigned int vSize;
        unsigned int iSize;
        unsigned int byteSize;
};


struct Acceleration {

    float forward;      // towards or away from center of view
    float strafe;       // sideways
    float ascend;       // ascend (descend if negative)
    float pitch;        // camera up and down rotation
    float yaw;          // camera left and right rotation

};


struct CamCoord {
    glm::vec3 pos;
    glm::vec3 front;
    glm::vec3 up;
};

struct Material {
    glm::vec3 ambient;
    glm::vec3 diffuse;
    glm::vec3 specular;
    float shininess;
};

struct Light {
    glm::vec3 position;
    
    glm::vec3 ambient;
    glm::vec3 diffuse;
    glm::vec3 specular;
    
    float constant;
    float linear;
    float quadratic;
};


class Mesh;
class Shader;
class Model;

using VertexVec = std::vector<std::shared_ptr<Vertex>>;
using IndexVec = std::vector<unsigned int>;
using NormalVec = std::vector<glm::vec3>;
using MeshVec = std::vector<Mesh*>;
using TexVec = std::vector<glm::vec2>;
using ShaderMap = std::unordered_map<std::string, Shader*>;
using ModelMap = std::unordered_map<std::string, Model*>;


#endif // DATASTRUCTURES_HH
