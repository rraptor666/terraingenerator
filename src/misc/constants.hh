#ifndef CONSTANTS_HH
#define CONSTANTS_HH

#include <glm/glm.hpp>
#include <string>
#include <vector>
#include <unordered_map>

// window
const std::string W_TITLE = "glTemplate";
const unsigned int W_WIDTH = 800;
const unsigned int W_HEIGHT = 600;

// camera
const glm::vec3 POS = glm::vec3(0,0,-3);
const float FOV = 70.0f;                                // field of vision
const float ASPECT = float(W_WIDTH)/float(W_HEIGHT);    // viewport dimensions
const float Z_NEAR = 0.1f;                              // distance to closer zy clip plane
const float Z_FAR = 1000.0f;                            // distance to farther zy clip plane

// shaders
const std::string TRI_FS = "triangle.fs";
const std::string TRI_VS = "triangle.vs";
const std::string SKYBOX_FS = "skybox.fs";
const std::string SKYBOX_VS = "skybox.vs";
const std::string CUBE_FS = "cube.fs";
const std::string CUBE_VS = "cube.vs";
const std::string TERRAIN_FS = "terrain.fs";
const std::string TERRAIN_VS = "terrain.vs";
const std::string LAMP_FS = "lamp.fs";
const std::string LAMP_VS = "lamp.vs";


const std::string SKYBOX_FS_120 = "skybox_120.fs";
const std::string SKYBOX_VS_120 = "skybox_120.vs";

const std::string DIR_120 = "./res/shaders/120/";
const std::string DIR_330 = "./res/shaders/330/";


// textures
const std::string WALL_LINE_TEX = "./res/textures/wall_line.png";
const std::string WALL_TEX = "./res/textures/wall.jpg";

const std::vector<std::string> LAKEBOX = {

    "./res/textures/lakecube/back.jpg",
    "./res/textures/lakecube/front.jpg",
    "./res/textures/lakecube/right.jpg",
    "./res/textures/lakecube/left.jpg",
    "./res/textures/lakecube/bottom.jpg",
    "./res/textures/lakecube/top.jpg",

};

// TODO: needs special loading options
const std::vector<std::string> SPACEBOX = {

    "./res/textures/spacecube/back.png",
    "./res/textures/spacecube/front.png",
    "./res/textures/spacecube/right.png",
    "./res/textures/spacecube/left.png",
    "./res/textures/spacecube/bottom.png",
    "./res/textures/spacecube/top.png",

};

// TODO: needs special loading options
const std::vector<std::string> SPACEBOX2 = {

    "./res/textures/spacecube2/back.png",
    "./res/textures/spacecube2/front.png",
    "./res/textures/spacecube2/right.png",
    "./res/textures/spacecube2/left.png",
    "./res/textures/spacecube2/bottom.png",
    "./res/textures/spacecube2/top.png",

};



#endif // CONSTANTS_HH
