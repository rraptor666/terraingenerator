#ifndef UTILITY_HH
#define UTILITY_HH

#include "datastructures.hh"
#include "../model/mesh.hh"
#include "../model/model.hh"

#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>


namespace utility {


/**
 * @brief split, splits string into vector of strings by "separator"
 * @param s, string to be split
 * @param separator
 * @param removeSpaces
 * @return split string
 */
void split(std::vector<std::string> &vec,
           std::string s,
           std::string separator,
           bool removeSpaces=false);


void genMeshes(MeshVec &m, const unsigned int meshCount, VertexVec &v);


// TODO: does not give right values
void printModelStats(const std::string &objName, ModelMap &models, std::string name="");

float map(const float value, 
          const float min1, const float max1, 
          const float min2, const float max2);


/**
 * @brief genSurfaceNormals, calculates normals for each triangle using cross product
 *        and then normalizing the result 
 * @param nVec, storage to be filled with normals
 * @param vVec, vertices from which normals are calculated
 * @param iVec, indices of vertices of vVec
 */
void genSurfaceNormals(NormalVec &nVec, VertexVec &vVec, IndexVec &iVec);

} // utility

#endif // UTILITY_HH
