#include "utility.hh"

namespace utility {


void split(std::vector<std::string> &vec,
           std::string s,
           std::string separator,
           bool removeSpaces) {

    std::string::size_type begin = 0;
    std::string::size_type end = 0;
    std::string element = "";

    while(end != std::string::npos) {

        end = s.find(separator, begin);
        element = s.substr(begin, end-begin);

        if (element != "" || !removeSpaces) {
            vec.push_back(element);
        }
        begin = end + separator.size();
    }

}


#include <iostream>
// TODO: what if meshCount > v.size()?
void genMeshes(MeshVec &m, const unsigned int meshCount, VertexVec &v) {

    if (meshCount == 0) {
        return;
    }

    unsigned int increment = v.size()/meshCount;
    unsigned int begin = 0;
    unsigned int end = increment;

    for (unsigned int i=0; i<meshCount+1; ++i) {

        VertexVec tmp;
        for (unsigned int j=begin; j<end; ++j) {
            
            if (j > v.size()-1) {
                break;
            }
            tmp.push_back( v[j] );

        }
        if (tmp.size() > 0) {
            m.push_back( new Mesh(tmp) );
        } else {
            break;
        }
        begin = end;
        end += increment;

        // if (end > v.size()) {
        //     end = v.size();
        // } 

        // std::cout << "vertices in the mesh: " << m[i]->getVertexCount() << std::endl;
    }

    // if (end < meshCount-1) {

    // }


}

void printModelStats(const std::string &objName, ModelMap &models, std::string name) {

    auto iter = models.find(objName);
    if (iter == models.end()) {
        std::cerr << "No model "
                  << objName
                  << " found.\n";
    
        return;
    }

    Stat* s = iter->second->getStat();
    s->byteSize += sizeof( *models[objName] );

    std::cout << "***"; 
    if (name != "") {
        std::cout << name; 
    } else {
        std::cout << objName;
    }
    std::cout << "***"
              << "\n  vertices: " << s->vSize 
              << "\n  indices: " << s->iSize 
              << "\n  size in bytes: " << s->byteSize  //sizeof( *(mModels["0"]) ) 
              << std::endl;
}


float map(const float value, 
          const float min1, const float max1, 
          const float min2, const float max2) {
              
    return min2 + ((max2 - min2) / (max1 - min1)) * (value - min1);
}


void genSurfaceNormals(NormalVec &nVec, VertexVec &vVec, IndexVec &iVec) {
    
    // std::cerr << "size: " << vVec.size() << "\n";
    unsigned int i = 0;
    while ( i < iVec.size() ) {
        
        // std::cerr << "i: " << i << "\n"; 
        
        glm::vec3 lhs(vVec[ iVec[i] ]->pos.x - vVec[ iVec[i+1] ]->pos.x, 
                      vVec[ iVec[i] ]->pos.y - vVec[ iVec[i+1] ]->pos.y, 
                      vVec[ iVec[i] ]->pos.z - vVec[ iVec[i+1] ]->pos.z);
                      
        glm::vec3 rhs(vVec[ iVec[i] ]->pos.x - vVec[ iVec[i+2] ]->pos.x, 
                      vVec[ iVec[i] ]->pos.y - vVec[ iVec[i+2] ]->pos.y, 
                      vVec[ iVec[i] ]->pos.z - vVec[ iVec[i+2] ]->pos.z);
                      
        nVec.push_back( glm::normalize( glm::cross(lhs, rhs) ) );
        
        i += 3;
    }

    
}


} // utility
