#include "game.hh"


Game::Game(int argc, char** argv):

    mArgc(argc),
    mArgv(argv),
    mInited(false),
    mShouldReGen(false),
    mWindow(new Window),
    mEvent(new SDL_Event),
    mInput( Input::getInstance()),
    mCam( new Camera(POS, FOV, ASPECT, Z_NEAR, Z_FAR, mInput->getAccel()) ),
    mCubeShader(nullptr),
    mTerrainShader(nullptr),
    mLampShader(nullptr),
    mTerrainGen( TerrainGenerator::getInstance() ),
    mNoiseVal(0.2f),
    mTerrainWidth(20.0f),
    mRoughness(2.0f),
    mOctaves(4),
    mResolution(100) {

        unsigned int t_0 = SDL_GetTicks();
        mInited = init();
        unsigned int t_1 = SDL_GetTicks();
        std::cout << "Init time: " << t_1 - t_0 << " ms\n";

}


Game::~Game() {

    delete mCam;
    delete mEvent;
    delete mWindow;

}


bool Game::isInited() {
    return mInited;
}


void Game::run() {

    float t_lastFrame = 0.0f;
    float t_currentFrame = 0.0f;
    float t_delta = 0.0f;
    unsigned int loopCount = 0;
    
    float rotAcc = 0.0f;
    float rotAmount = 0.01f;
    float r = 2.0f;

    // update loop
    while ( mInput->handleInput(mEvent, this) ) {
        
        t_currentFrame = SDL_GetTicks();
        t_delta = t_currentFrame - t_lastFrame;
        t_lastFrame = t_currentFrame;

        mWindow->clear();

        // draw models
        // mModels["cube"]->draw(mCam);
        // mModels["lamp"]->draw(mCam);
        mModels["terrain"]->draw(mCam);
        // mModels["sphere"]->draw(mCam);
        

        // update FPS counter
        if (loopCount % 100 == 0) {
            mWindow->updateFPSCounter(t_delta);
            loopCount = 0;
            // TODO: http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-11-2d-text/
        }

        // update lamp position
        mModels["lamp"]->update(t_delta, r * glm::vec3( cos(rotAcc), 0.0f, sin(rotAcc) ));
        mModels["cube"]->getLight()->position = mModels["lamp"]->getPos();
        

        // update camera view
        mCam->updateView(t_delta);


        // render present
        mWindow->swapBuffers();

        ++loopCount;
        rotAcc += rotAmount;
    }


}


bool Game::init() {
    
    // init models
    VertexVec cubeVertices =  {
        // back
        std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f, -0.5f, -0.5f),  glm::vec2(0.0f, 0.0f), glm::vec3(0.0f, 0.0f, -1.0f)}),
        std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f, -0.5f, -0.5f),  glm::vec2(1.0f, 0.0f), glm::vec3(0.0f, 0.0f, -1.0f)}),
        std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f,  0.5f, -0.5f),  glm::vec2(1.0f, 1.0f), glm::vec3(0.0f, 0.0f, -1.0f)}),

        std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f,  0.5f, -0.5f),  glm::vec2(1.0f, 1.0f), glm::vec3(0.0f, 0.0f, -1.0f)}),
        std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f,  0.5f, -0.5f),  glm::vec2(0.0f, 1.0f), glm::vec3(0.0f, 0.0f, -1.0f)}),
        std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f, -0.5f, -0.5f),  glm::vec2(0.0f, 0.0f), glm::vec3(0.0f, 0.0f, -1.0f)}),

        // front
        std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f, -0.5f,  0.5f),  glm::vec2(0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f)}),
        std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f, -0.5f,  0.5f),  glm::vec2(1.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f)}),
        std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f,  0.5f,  0.5f),  glm::vec2(1.0f, 1.0f), glm::vec3(0.0f, 0.0f, 1.0f)}),

        std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f,  0.5f,  0.5f),  glm::vec2(1.0f, 1.0f), glm::vec3(0.0f, 0.0f, 1.0f)}),
        std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f,  0.5f,  0.5f),  glm::vec2(0.0f, 1.0f), glm::vec3(0.0f, 0.0f, 1.0f)}),
        std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f, -0.5f,  0.5f),  glm::vec2(0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f)}),

        // right
        std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f,  0.5f,  0.5f),  glm::vec2(0.0f, 0.0f), glm::vec3(1.0f, 0.0f, 0.0f)}),
        std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f,  0.5f, -0.5f),  glm::vec2(1.0f, 0.0f), glm::vec3(1.0f, 0.0f, 0.0f)}),
        std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f, -0.5f, -0.5f),  glm::vec2(1.0f, 1.0f), glm::vec3(1.0f, 0.0f, 0.0f)}),

        std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f, -0.5f, -0.5f),  glm::vec2(1.0f, 1.0f), glm::vec3(1.0f, 0.0f, 0.0f)}),
        std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f, -0.5f,  0.5f),  glm::vec2(0.0f, 1.0f), glm::vec3(1.0f, 0.0f, 0.0f)}),
        std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f,  0.5f,  0.5f),  glm::vec2(0.0f, 0.0f), glm::vec3(1.0f, 0.0f, 0.0f)}),

        // left
        std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f,  0.5f,  0.5f),  glm::vec2(0.0f, 0.0f), glm::vec3(-1.0f, 0.0f, 0.0f)}),
        std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f,  0.5f, -0.5f),  glm::vec2(1.0f, 0.0f), glm::vec3(-1.0f, 0.0f, 0.0f)}),
        std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f, -0.5f, -0.5f),  glm::vec2(1.0f, 1.0f), glm::vec3(-1.0f, 0.0f, 0.0f)}),

        std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f, -0.5f, -0.5f),  glm::vec2(1.0f, 1.0f), glm::vec3(-1.0f, 0.0f, 0.0f)}),
        std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f, -0.5f,  0.5f),  glm::vec2(0.0f, 1.0f), glm::vec3(-1.0f, 0.0f, 0.0f)}),
        std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f,  0.5f,  0.5f),  glm::vec2(0.0f, 0.0f), glm::vec3(-1.0f, 0.0f, 0.0f)}),

        // bottom
        std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f, -0.5f, -0.5f),  glm::vec2(0.0f, 0.0f), glm::vec3(0.0f, -1.0f, 0.0f)}),
        std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f, -0.5f, -0.5f),  glm::vec2(1.0f, 0.0f), glm::vec3(0.0f, -1.0f, 0.0f)}),
        std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f, -0.5f,  0.5f),  glm::vec2(1.0f, 1.0f), glm::vec3(0.0f, -1.0f, 0.0f)}),

        std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f, -0.5f,  0.5f),  glm::vec2(1.0f, 1.0f), glm::vec3(0.0f, -1.0f, 0.0f)}),
        std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f, -0.5f,  0.5f),  glm::vec2(0.0f, 1.0f), glm::vec3(0.0f, -1.0f, 0.0f)}),
        std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f, -0.5f, -0.5f),  glm::vec2(0.0f, 0.0f), glm::vec3(0.0f, -1.0f, 0.0f)}),

        // top
        std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f,  0.5f, -0.5f),  glm::vec2(0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f)}),
        std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f,  0.5f, -0.5f),  glm::vec2(1.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f)}),
        std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f,  0.5f,  0.5f),  glm::vec2(1.0f, 1.0f), glm::vec3(0.0f, 1.0f, 0.0f)}),

        std::make_shared<Vertex> (Vertex{glm::vec3( 0.5f,  0.5f,  0.5f),  glm::vec2(1.0f, 1.0f), glm::vec3(0.0f, 1.0f, 0.0f)}),
        std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f,  0.5f,  0.5f),  glm::vec2(0.0f, 1.0f), glm::vec3(0.0f, 1.0f, 0.0f)}),
        std::make_shared<Vertex> (Vertex{glm::vec3(-0.5f,  0.5f, -0.5f),  glm::vec2(0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f)})
    };
    mModels.insert( {"cube", new Model(cubeVertices, 
                                       glm::vec3(0.0f, 5.0f, 0.0f),
                                       glm::vec3(0.0f),
                                       glm::vec3(1.0f),
                                       Asset::loadTexture(WALL_LINE_TEX))} );
                            
                        

    mModels.insert( {"lamp", new Model(cubeVertices,
                                       glm::vec3(0.0f, 5.0f, 0.0f),
                                       glm::vec3(0.0f),
                                       glm::vec3(0.25f))} );
                                       
    VertexVec v;
    IndexVec i;    
    // mTerrainGen->genSphereCube(v, i, 10, 10.0f);     
    // for (unsigned int j=0; j<i.size(); ++j) {
    //     std::cout << v[i[j]]->pos.x << ", " << v[i[j]]->pos.y << ", " << v[i[j]]->pos.z << "\n";
    // }  
    std::cout << v.size() << ", " << i.size() << "\n";                    
    mModels.insert( {"sphere", new Model(v, i, 
                                         glm::vec3(0.0f, 2.0f, 0.0f), 
                                         glm::vec3(0.0f),
                                         glm::vec3(1.0f),
                                         0)} );                               

    VertexVec vVec;
    IndexVec iVec;
    NormalVec nVec;
    mTerrainGen->genTerrain(vVec, iVec, mTerrainWidth, mRoughness, mResolution, mOctaves);
    utility::genSurfaceNormals(nVec, vVec, iVec);
    
    mModels.insert( {"terrain", new Model(vVec, iVec, nVec,
                                          glm::vec3(0.0f),
                                          glm::vec3(0.0f),
                                          glm::vec3(1.0f),
                                          Asset::loadTexture(WALL_TEX))} );
    
    Material* tMaterial = new Material{
        glm::vec3(1.0f, 0.5f, 0.3f), 
        glm::vec3(1.0f, 0.5f, 0.3f), 
        glm::vec3(0.5f), 
        32.0f
    };

    Light* tLight = new Light{
        mModels["lamp"]->getPos(),
        glm::vec3(1.0f),
        glm::vec3(5.8f),
        glm::vec3(1.0f),
        1.0f,
        0.0014f,
        0.00007f
    };

    mModels["terrain"]->setMaterial(tMaterial);
    mModels["terrain"]->setLight(tLight);
    // mModels["terrain"]->setNormalsAt(nVec, 0);

    mModels["cube"]->setMaterial(tMaterial);
    mModels["cube"]->setLight(tLight);
    
    // init shaders
    mModels["cube"]->setShader( new Shader(DIR_120, CUBE_FS, CUBE_VS) );
    mModels["terrain"]->setShader( new Shader(DIR_120, TERRAIN_FS, TERRAIN_VS) );
    mModels["lamp"]->setShader( new Shader(DIR_120, LAMP_FS, LAMP_VS) );


    return true;
}


void Game::regenTerrain(const char* keyPressed) {
    
    std::string str(keyPressed);
    
    Material* m = mModels["terrain"]->getMaterial();
    Light* l = mModels["terrain"]->getLight(); 
    Shader* s = mModels["terrain"]->getShader();
    delete mModels["terrain"];
    
    VertexVec vVec;
    IndexVec iVec;
    NormalVec nVec;
    
    
    if (str == "up")                                   mRoughness += 0.1f; 
    else if (str == "down")                            mRoughness -= 0.1f;
        
        if (mRoughness < -0.1f) mRoughness = -0.1f;
    
    else if (str == "left" && mOctaves > 1)            --mOctaves;
    else if (str == "right")                           ++mOctaves;
    else if (str == "pageup")                          mResolution += 10;
    else if (str == "pagedown" && mResolution >= 20)   mResolution -= 10;
    else if (str == "home")                            mTerrainWidth += 10.0f;
    else if (str == "end" && mTerrainWidth > 10.0f)    mTerrainWidth -= 10.0f;
    else if (str == "insert")                          mNoiseVal += 0.01f;
    else if (str == "delete" && mNoiseVal > 0.01f)     mNoiseVal -= 0.01f;
    
    
    
    std::cout << "roughness: "     << mRoughness 
              << ", octaves: "     << mOctaves 
              << ", width: "       << mTerrainWidth
            //   << ", resolution: " << resolution
              << ", max height: "  << MAX_HEIGHT
              << ", noise const: " << mNoiseVal
              << "\n";
    
    mTerrainGen->setNoiseFactor(mNoiseVal);
    mTerrainGen->genTerrain(vVec, iVec, mTerrainWidth, mRoughness, mResolution, mOctaves);
    utility::genSurfaceNormals(nVec, vVec, iVec);
    
    mModels["terrain"] = new Model(vVec, iVec, nVec,
                                   glm::vec3(0.0f),
                                   glm::vec3(0.0f),
                                   glm::vec3(1.0f),
                                   Asset::loadTexture(WALL_TEX));
    
    mModels["terrain"]->setMaterial(m);
    mModels["terrain"]->setLight(l);
    mModels["terrain"]->setShader(s);
    // mModels["terrain"]->setNormalsAt(nVec, 0);
}
