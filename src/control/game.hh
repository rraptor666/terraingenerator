#ifndef GAME_HH
#define GAME_HH

#include "camera.hh"
#include "input.hh"
#include "../misc/datastructures.hh"
#include "../model/asset.hh"
#include "../model/model.hh"
#include "../model/terraingenerator.hh"
#include "../view/shader.hh"
#include "../view/window.hh"

#include <sstream>
#include <unordered_map>



class Game {

public:

    Game(int argc, char** argv);
    ~Game();

    bool isInited();
    void run();
    void regenTerrain(const char* keyPressed);


private:

    bool init();
    
    // for run time tweaking
    int mArgc;
    char** mArgv;
    
    bool mInited;
    bool mShouldReGen;

    Window* mWindow;
    SDL_Event* mEvent;

    Input* mInput;
    Camera* mCam;

    ModelMap mModels;

    Shader* mCubeShader;
    Shader* mTerrainShader;
    Shader* mLampShader;

    TerrainGenerator* mTerrainGen;
    
    float mNoiseVal;
    float mTerrainWidth;
    float mRoughness;
    unsigned int mOctaves;
    unsigned int mResolution;
    

};

#endif // GAME_HH
