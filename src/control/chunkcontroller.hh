#ifndef CHUNKCONTROLLER_HH
#define CHUNKCONTROLLER_HH

#include "../model/model.hh"
#include "camera.hh"
#include <deque>


const unsigned int RENDER_DIST = 4;  // distance (in chunks) to the farthest chunk
// const unsigned int GEN_DIST = 20;    // max chunks generated per direction

// TODO: change deque to list?

struct Chunk {
    Model* model;
    bool visible;
};

class ChunkController {
    
public:
    static ChunkController* getInstance();
    void release();

    void genChunks(float chunkWidth);
    void drawChunks(Camera* cam);
    
    // void genProcedually();
 
    
private:    
    ChunkController();
    
    static ChunkController* mInstance;
    
    std::deque<Chunk> mChunks;
    
    
    
    
};


#endif