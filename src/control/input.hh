#ifndef INPUT_HH
#define INPUT_HH

#include "../misc/datastructures.hh"

#include <SDL2/SDL.h>

const float MOVE_CONST = 1.0f;
const float CAM_CONST = 1.0f;  // w/o vsync: 0.01f, w/ 1.0f 

class Game;

class Input {

public:
    static Input* getInstance();

    /**
     * @brief handleInput, contains core functionality for polling user input
     * @param e, SDL's eventhandler object
     * @param obj, enables callback to control object
     */
    bool handleInput(SDL_Event *e, Game* obj);
    Acceleration* getAccel();


private:
    Input();
    void terrainInput(const Uint8* keyStates, Game* obj);
    void movementInput(const Uint8* keyStates);
    void cameraInput(const Uint8* keyStates);

    static Input* mInstance;
    Acceleration* mAccel;

};


#endif