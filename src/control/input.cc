#include "input.hh"
#include "game.hh"

Input* Input::mInstance = nullptr;

Input* Input::getInstance() {

    if (mInstance == nullptr) {
        mInstance = new Input;
    }

    return mInstance;
}


bool Input::handleInput(SDL_Event *e, Game* obj) {

    while ( SDL_PollEvent(e) ) {

        const Uint8* keyStates = SDL_GetKeyboardState(NULL);

        // mouse click on "close window"-icon or "q"
        if (e->type == SDL_QUIT || keyStates[SDL_SCANCODE_Q]) {
            return false;
        }
        
        terrainInput(keyStates, obj);
        movementInput(keyStates);
        cameraInput(keyStates);
    }

    return true;
}


Acceleration* Input::getAccel() {
    return mAccel;
}



Input::Input():
    mAccel( new Acceleration({0.0f, 0.0f, 0.0f, 0.0f, 0.0f}) ) {

}


void Input::terrainInput(const Uint8* keyStates, Game* obj) {
    
    // UP increment roughness
    // DOWN decrement roughness
    // RIGHT increment octaves
    // LEFT decrement octaves
    // PAGEUP increment squares per terrain side
    // PAGEDOWN decrement squares 
    if (keyStates[SDL_SCANCODE_UP])            obj->regenTerrain("up");
    else if (keyStates[SDL_SCANCODE_DOWN])     obj->regenTerrain("down");
    else if (keyStates[SDL_SCANCODE_RIGHT])    obj->regenTerrain("right");
    else if (keyStates[SDL_SCANCODE_LEFT])     obj->regenTerrain("left");
    else if (keyStates[SDL_SCANCODE_PAGEUP])   obj->regenTerrain("pageup");
    else if (keyStates[SDL_SCANCODE_PAGEDOWN]) obj->regenTerrain("pagedown");
    else if (keyStates[SDL_SCANCODE_HOME])     obj->regenTerrain("home");
    else if (keyStates[SDL_SCANCODE_END])      obj->regenTerrain("end");
    else if (keyStates[SDL_SCANCODE_INSERT])   obj->regenTerrain("insert");
    else if (keyStates[SDL_SCANCODE_DELETE])   obj->regenTerrain("delete");
}


void Input::movementInput(const Uint8* keyStates) {

    // W forward
    // S backwards
    if (keyStates[SDL_SCANCODE_W])      mAccel->forward =  1.0f;
    else if (keyStates[SDL_SCANCODE_S]) mAccel->forward = -1.0f;
    else                                mAccel->forward =  0.0f;

    mAccel->forward *= MOVE_CONST;

    // SPACE ascend
    // SHIFT descend
    if (keyStates[SDL_SCANCODE_SPACE])       mAccel->ascend =  1.0f;
    else if (keyStates[SDL_SCANCODE_LSHIFT]) mAccel->ascend = -1.0f;
    else                                     mAccel->ascend =  0.0f;

    mAccel->ascend *= MOVE_CONST;

    // A strafe left
    // D strafe right
    if (keyStates[SDL_SCANCODE_A])      mAccel->strafe = -1.0f;
    else if (keyStates[SDL_SCANCODE_D]) mAccel->strafe =  1.0f;
    else                                mAccel->strafe =  0.0f;
    
    
    mAccel->strafe *= MOVE_CONST;
    
    
    // super speed
    if (keyStates[SDL_SCANCODE_LCTRL]) {
        mAccel->forward *= 6.0f;
        mAccel->strafe  *= 6.0f;
        mAccel->ascend  *= 6.0f;
    }

}


void Input::cameraInput(const Uint8* keyStates) {

    // I pitch up
    // K pitch down
    if (keyStates[SDL_SCANCODE_I])      mAccel->pitch =  1.0f;
    else if (keyStates[SDL_SCANCODE_K]) mAccel->pitch = -1.0f;
    else                                mAccel->pitch =  0.0f;

    mAccel->pitch *= CAM_CONST;

    // J yaw left
    // L yaw right
    if (keyStates[SDL_SCANCODE_J])      mAccel->yaw = -1.0f;
    else if (keyStates[SDL_SCANCODE_L]) mAccel->yaw =  1.0f;
    else                                mAccel->yaw =  0.0f;

    mAccel->yaw *= CAM_CONST;

}

