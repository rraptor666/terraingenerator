
OBJS = src/*.cc \
	   src/control/*.cc \
	   src/misc/*.cc \
	   src/model/*.cc \
	   src/view/*.cc 

CC = g++

LINKER_FLAGS = -lSDL2 -lGLEW -lGL

COMPILER_FLAGS = -std=c++11 -Wall

OBJ_NAME = terrain
DB_NAME = debug

all : $(OBJS) ; $(CC) $(COMPILER_FLAGS) $(LINKER_FLAGS) $(OBJS) -o $(OBJ_NAME) 

debug : $(OBJS) ; $(CC) $(COMPILER_FLAGS) -g $(LINKER_FLAGS) $(OBJS) -o $(DB_NAME)
